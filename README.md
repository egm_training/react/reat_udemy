# Notes

## Section — Basic commands

- Create an application
  ```bash
  npx creates-react-app mon app
  ```
- Start a React application
  ```bash
  npm start
  ```
- Stop the server
  ```bash
  ^(ctrl) + c
  ```

## Section 2 - JSX

- JSX is and special dialect of JavaScript, seems like HTML but is only to simplify the React dialect.
- In JSX all is a component. For example for inline styling:

  ```html
  <button style="background-color: blue , color: white">Textbutton</button>
  ```

  ```jsx
  <button style={{ backgroundColor: "blue", color: "white" }}>
    Textbutton
  </button>
  ```

  We can put double or single quotes for properties. The convention is to use double quotes.

- The '{}' indicates a JavaScript variable or object.

  - It can be used to get a variable comming from a function

    ```jsx
    <button style={{ backgroundColor: "blue", color: "white" }}>
      {getName()}
    </button>
    ```

  - We can declare an object like this

    ```javascript
    const buttonText = { text: "Click Me" };
    ```

    and use it calling the attributes :

    ```jsx
    <button style={{ backgroundColor: "blue", color: "white" }}>
      {buttonText.text}
    </button>
    ```

- There are reserved names as:

  | HTML  | JSX       |
  | ----- | --------- |
  | Class | className |
  | for   | htmlFor   |

- Comments:

  - Outside a React component

    ```javascript
    /* This is a comment outside */
    ```

  - Inside we need to use {} as for other variables

    ```javascript
    {
      /* This is a comment outside */
    }
    ```

## Section 3 - Communication with Props

A good site to look for styles is : [Semantic UI](https://semantic-ui.com/views/comment.html). However, documentation is not great at the moment. We can search for the link to the library using [cdnjs](https://cdnjs.com).

To link the library to the html, we simply provide the link in the index :

```html
<link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css"
/>
```

We can use fake data to test our components using the library [Faker](https://github.com/marak/Faker.js/). For this we need to install the library in our local project:

```bash
npm install --save faker
```

And import into the index.js

```jsx
import Faker from "faker";
```

We can access to the generator with :

```jsx
src={Faker.image.abstract()}
```

### Reusability

1. Identify the JSX code to be duplicated
2. Identify the purpose of this block
3. Move the component to a new file
4. Make the new component configurable

**Example: A comment**

1.  The JSX code
    ```jsx
    <div className="comment">
      <a href="/" className="avatar">
        <img alt="avatar" src={Faker.random.image()} />
      </a>
      <div className="content">
        <div className="author">
          <a href="/" className="author">
            Sam
          </a>
        </div>
        <div className="metadata">
          <span className="date">Today at 9:00PM</span>
        </div>
        <div className="text">Nice blog from component</div>
      </div>
    </div>
    ```
1.  The purpose of the block.

    - Create a new comment component

1.  Create a new component : file name CommentDetail.js.

    ```jsx
    import React from "react"; // Import the react library
    import Faker from "faker";
    const CommentDetail = () => {
      return {
        /* Copy the code from step number 1*/
      };
    };
    ```

1.  Make the link between the index.js and this component.
    - Add the export command to the component file.
    ```jsx
    export default CommentDetail;
    ```
    - Add the import command to the index file.
    ```jsx
    import CommentDetail from "./CommentDetail";
    ```

### Communication

The CommentDetail component is now the _child component_ of the App (The parent). To communication information between the parent and the child we use the _Props System_.

The component object can be build using properties(constructor). Typical definition is :

```jsx
const ComponentName = (props) => { return (); };
```

props is list of keys (properties). Ex:

```jsx
props = { key1 = "value 1" key2 = "value 2"}
```

Properties can be examined on the browser using:

```jsx
console.log(props);
```

And finally we can pass JavaScript variables:

```javascript
props = {author = "Birus" avatar = {Faker.random.image()}}
```

### Nesting children

We can pass children instances to other components using a similar structure to div.

```jsx
<ApprovalCard>
  {/* A children component of App */}
  <CommentDetail
  {/* A children component of App passed to Approval card as props*/}
    author="Lalosman"
    datePost="Today at 5h00"
    msgPost="Me too"
    imgPost={Faker.random.image()}
  />
</ApprovalCard>
```

Inside the ApprovalCard component, the instance of the comment is passed as _children_

```jsx
const ApprovalCard = (props) => {
  console.log(props);
  return (
    <div className="ui card">
      <div className="content">{props.children}</div>
    </div>
  );
};
```

If several children are passed, children property is an Array. We can pass also div and other stuff:

```jsx
<ApprovalCard>
  <div>
    <h4>Warning</h4>
    Are you sure ?
  </div>
</ApprovalCard>
```

## Section 4 — Class base components

A component can be a function or a class.

```mermaid
graph LR
A[Component]
D(HTML code)
A --> B(Function) --> D
A --> C(Class) --> D
```

- Functions:
  - Generate JSX
- Classes:
  - Generate JSX
  - State system
  - Lifecycle

Strategy of the course

```mermaid
graph LR
A(Learn class components)-->B(Learn Hooks )-->C(Learn Redux)
```

### Season application

#### Challenges

- Physical location

  - Using the geolocalisation API see [Mozilla](https://developer.mozilla.org/fr/docs/Web/API/Geolocation_API)

  ```jsx
  window.navigator.geolocation.getCurrentPosition(
    (position) => console.log(position),  {/* Successful call */}
    (err) => console.log(err)             {/* Error catching  */}
  );
  ```

  - This option is not available using a function component beacuase the timestamp is long. We need to use a **Class**
  - We need to use **States**

**Class**

Similar to java definition of classes

Function :

```javascript
const App = () => {
  //some code
  return ( something)
  );
};
```

Class:

```javascript
class className extends React.component {
  constructor() {
    // calling the super constructor parent
    super();
  }
  // render method definition REQUIRED
  render() {
    //some code
    return something;
  }
}
```

**States**

> A state is a JavaScript object that contains data relevant to a component.

For example the _latitude_ of position.

> Re-render → update the state.
> An state can be **ONLY** be updated using **setState**

Rules:

- Only usable with classes.
- The state must be initialized in the constructor, using the component variable state of the object :`this.state`.

  ```jsx
  class className extends React.component {
    constructor() {
      // calling the super constructor parent
      super();
      // setting the state objets
      this.state = { state_example: null };
    }
    // render method definition REQUIRED
    render() {
      //some code
      return something;
    }
  }
  ```

```mermaid
graph LR; A(JS)-->B(Instance of App)-->C(Call the constructor)-->D(initialize state);
```

- Update the state using setState. This function has been defined in the react component class.

  ```jsx
    // We can define the constructor
    constructor(props) {
      super(props);
      // setting the state objets
      // only time to do direct assignment
      this.state = { lat: 40 };

      // Getting current location
      // This part is gonna be executed only after the geolocalisation works
      window.navigator.geolocation.getCurrentPosition(
        (position) => {
          this.setState({ lat: position.coords.latitude });
        },
        (err) => console.log(err)
      );
    }
  ```

  The state is updated using callback functions. The state is only updated after the callback function is called. In this way the page is rendered even if the states variables are not set.

**Conditionally rendering**

| Response | Error ? | Action        |
| -------- | ------- | ------------- |
| OK       | NO      | Show response |
| NO       | NO      | Show error    |
| NO       | NO      | Show waiting  |

use `if`an state value different from `null`

```jsx
  render() {
    if (this.state.errorMsg && !this.setState.lat) {
      return (
        <div>
          <SeasonDisplay />
          <div>Error: {this.state.errorMsg}</div>
        </div>
      );
    }
    if (!this.state.errorMsg && this.setState.lat) {
      return (
        <div>
          <SeasonDisplay />
          <div>Latitude: {this.state.lat}</div>
        </div>
      );
    }
    return <div>Waiting</div>;
  }
```
