// Import the react and react dom
import React from "react"; // React is a variable , react is the dependency.
import ReactDOM from "react-dom"; // ReactDOM is a variable

// Function definition out of the component
function getButtonText() {
  return "Click on Me (function)";
}

function getTime() {
  return new Date().toLocaleTimeString();
}

// Create a react component

const App = () => {
  const buttonText = "Click on Me!";
  const buttonTextO = { text: "Click Me Object" };
  const myStyle = { backgroundColor: "black", color: "white" };

  return (
    <div>
      <h2>An example using a component</h2>
      {/* This is a comment inside react*/}
      <label className="label" for="name">
        Enter name
      </label>
      <input id="name"></input>
      <button style={{ backgroundColor: "blue", color: "white" }}>
        {buttonText}
      </button>
      <br></br>
      <h2>An example using information from a function </h2>
      <label className="label" for="name">
        Enter name
      </label>
      <input id="name"></input>
      <button style={{ backgroundColor: "blue", color: "white" }}>
        {getButtonText()}
      </button>
      <br></br>
      <h2>An example passing objects</h2>
      <label className="label" htmlFor="name">
        Enter name
      </label>
      <input id="name"></input>
      <button style={myStyle}>{buttonTextO.text}</button>
      <br></br>
      <h2>An example of another function</h2>
      <label className="label" htmlFor="name">
        Current time is : {getTime()}
      </label>
    </div>
  );
};

// Take a react component and show it in the screen
ReactDOM.render(<App />, document.querySelector("#root"));
