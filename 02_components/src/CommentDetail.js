import React from "react"; // Import the react library

// Component function = Arguments
const CommentDetail = (props) => {
  // sen the props to the browser console
  console.log(props);
  return (
    <div className="comment">
      <a href="/" className="avatar">
        <img alt="avatar" src={props.imgPost} />
      </a>
      <div className="content">
        <div className="author">
          <a href="/" className="author">
            {props.author}
          </a>
        </div>
        <div className="metadata">
          <span className="date">{props.datePost}</span>
        </div>
        <div className="text">{props.msgPost}</div>
      </div>
    </div>
  );
};

export default CommentDetail;
