import React from "react";
import ReactDOM from "react-dom";
import Faker from "faker";
import CommentDetail from "./CommentDetail";
import ApprovalCard from "./ApprovalCard";

const App = () => {
  return (
    <div className="ui container comments">
      {/* Creating a comment manually*/}
      <div className="comment">
        <a href="/" className="avatar">
          <img alt="avatar" src={Faker.image.abstract()} />
        </a>
        <div className="content">
          <div className="author">
            <a href="/" className="author">
              Sam
            </a>
          </div>
          <div className="metadata">
            <span className="date">Today at 9:00PM</span>
          </div>
          <div className="text">Nice blog</div>
        </div>
      </div>

      {/* Creating a comment using our component*/}
      <CommentDetail
        author="Birus"
        datePost="Today at 1h00"
        msgPost="Love you lalito"
        imgPost={Faker.random.image()}
      />
      {/*    (key)name prop= value*/}
      <CommentDetail
        author="Lalosman"
        datePost="Today at 5h00"
        msgPost="Me too"
        imgPost={Faker.random.image()}
      />

      {/* Creating an approval card using our component*/}
      <ApprovalCard>
        <div>
          <h4>Warning</h4>
          Are you sure ?
        </div>
      </ApprovalCard>

      {/* Creating an approval card using nesting*/}
      <ApprovalCard>
        <CommentDetail
          author="Lalosman"
          datePost="Today at 5h00"
          msgPost="Me too"
          imgPost={Faker.random.image()}
        />
      </ApprovalCard>
    </div>
  );
};

ReactDOM.render(<App />, document.querySelector("#root"));
