import React from "react";
import ReactDOM from "react-dom";
import SeasonDisplay from "./SeasonDisplay";

/* THE FUNCTIONAL COMPONENT
const App = () => {
  // Getting current location
  window.navigator.geolocation.getCurrentPosition(
    (position) => console.log(position),
    (err) => console.log(err)
  );
  return (
    <div>
      <SeasonDisplay />
      <div>Latitude : </div>
    </div>
  );
};
*/

// THE CLASS COMPONENT
class App extends React.Component {
  // We can define the constructor
  constructor(props) {
    super(props);
    // setting the state objets
    // only time to do direct assignment
    this.state = { lat: null, errorMsg: "" };

    // Getting current location
    window.navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({ lat: position.coords.latitude });
      },
      (err) => {
        this.setState({ errorMsg: err.message });
      }
    );
  }

  // We have to define render - REQUIRED
  render() {
    if (this.state.errorMsg && !this.setState.lat) {
      return (
        <div>
          <SeasonDisplay />
          <div>Error: {this.state.errorMsg}</div>
        </div>
      );
    }
    if (!this.state.errorMsg && this.setState.lat) {
      return (
        <div>
          <SeasonDisplay />
          <div>Latitude: {this.state.lat}</div>
        </div>
      );
    }
    return <div>Waiting</div>;
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
